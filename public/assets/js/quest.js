function template() {
    var questions = Array();
    
    questions[0] = ``;
    generate(questions, slot);
}

function testQuestion(){
    var questions = Array();
    
    questions[0] = `
1.Siapakah bapa pembangunan modal insan ?
    a.	Tun Abdullah Ahmad Badawi
    b.	Tun Hussein Onn
    c.	Dato’ Sri Najib
    d.	Tun Dr Mahathir`;
    questions[1] = `
2.Apakah bunga kebangsaan Malaysia ?
    a.	Bunga Raya
    b.	Bunga Ros
    c.	Bunga Kertas
    d.	Bunga Mawar
`;
    questions[2] = `
3.Apakah ibu negeri Perlis ?
    a.	Kuala Perlis
    b.	Kangar
    c.	Arau
    d.	Padang Besar
`;
    questions[3] = `
4.Apakah planet yang terbesar dalam sistem suria ?
    a.	Marikh
    b.	Uranus
    c.	Zuhal
    d.	Musytari
`;
    questions[4] = `
5.Berapakah jumlah pemain dalam satu pasukan Futsal ?
    a.	5 orang
    b.	9 orang
    c.	7 orang
    d.	11 orang
`;
    questions[5] = `
6.Gambar 1 menunjukkan sebahagian daripada Tembok Besar China.
    Mengapakah tembok tersebut dibina?
    A.	Menandakan sempadan negeri		
    B.	Menjamin keselamatan wilayah
    C.	Menjadi benteng penghalang wilayah
    D.	Melambangkan keagungan kerajaan
`;  
    generate(questions,"test-");
}

function slot1(){
    var questions = Array();
    
    questions[0] = `
1.Berikut yang manakah merupakan rukun Negara Malaysia ?
    i.	Kepercayaan kepada tuhan
    ii.	Kesetiaan kepada agama dan negara
    iii.Kedaulatan undang-undang
    iv.	Kesopanan dan kesusilaan
    
    A.	i,ii dan iv
    B.	ii,iii dan iv
    C.	i,iii dan iv
    D.	Semua diatas
`;
    questions[1] = `
2.Antara berikut , yang manakah merupakan tokoh pejuang
  tempatan negeri Sabah?
    A.	Rentap
    B.	Mat Salleh
    C.	Shariff Masahor
    D.	Datu Patinggi Abang Haji Abdillah
`;
    questions[2] = `
3.Apakah planet yang terdekat dengan matahari ?
    A.	Utarid
    B.	Neptun
    C.	Zuhrah 
    D.	Marikh
`;
    questions[3] = `
4.Sukan Chinlone bagi kontijen Malaysia telah meraih pingat emas
  yang pertama semasa acara Sukan Sea ke 2017.Berapakah bilangan
  pemain dalam sukan tersebut?
    A.	5 orang
    B.	6 orang
    C.	11 orang
    D.	12 orang
`;
    questions[4] = `
5.Siapakah yang telah menciptakan peranti elektronik iaitu
  televisyen?
    A.	Alexander Graham Bell
    B.	John Logie Baird
    C.	Charles Babbage
    D.	Ben Franklin
`;
    questions[5] = `
6.Apakah fenomena yang berlaku berdasarkan rajah di atas ?
    A.	Ribut taufan
    B.	Kejadian malam
    C.	Aurora
    D.	Musim Sejuk
`;
    questions[6] = `
7.Persamaan Q = ML adalah ungkapan untuk
    A.	Tegangan permukaan 
    B.	Daya apungan
    C.	Haba Pendam
    D.	Kemuatan
`;
    questions[7] = `
8.Pada tahun berapakah, sejarah perang  dunia pertama tercetus ?
    A.	1914
    B.	1916
    C.	1918
    D.	1920
`;
    questions[8] = `
9.Antara berikut, negara manakah yang tidak tersenarai dalam
  Pertubuhan Negara-Negara Asia Tenggara (ASEAN) ?
    A.	Laos
    B.	China
    C.	Cambodia
    D.	Viet Nam
`;
    questions[9] = `
10.Perkataan “almari” dan “tuala” diambil daripada
   perkataan ________.
    A.	Jepun
    B.	Dutch
    C.	Portugis
    D.	British
`;
    questions[10] = `
11.Bilakah Uitm dinaiktaraf menjadi sebuah Universiti?
    A.	1998
    B.	1999
    C.	2000
    D.	2001
`;
    questions[11] = `
12.Siapakah Sultan Pahang terkini?
    A.	Sultan Haji Ahmad Shah Al-Musta’in Billah
    B.	Tengku Mahkota Tengku Abdullah  Al-Haj
    C.	Sultan Sharafuddin Idris Shah Alhaj
    D.	Sultan Nazrin Muizuddin Shah
`;
    questions[12] = `
13.Dimanakah ibu pejabat penerbit buku terkenal Thomas Reutars ?
    A.	London, UK
    B.	New York, Amerika Syarikat
    C.	Alphen aan den Rijn, Belanda
    D.	Changsa, China
`;
    questions[13] = `
14.Apakah motto utama Universiti Teknologi Mara ?
    A.	Tegas, Adil, Berhemah
    B.	Amanah, Tegas, Adil
    C.	Kami sedia membantu
    D.	Usaha, Taqwa, Mulia
`;
    questions[14] = `
15.Namakan kampus pertama yang dibina berkonsepkan
   Inisiatif Pembiayaan Swasta (PFI)
    A.	Kampus Seremban 3
    B.	Kampus Raub
    C.	Kampus Jasin
    D.	Kampus Rembau
`;
    questions[15] = `
16.Apakah elemen yang tidak terdapat pada jata negara ?
    A.	Harimau
    B.	Keris
    C.	Bunga Raya
    D.	Pedang
`;
    generate(questions,"slot1-");
}

function slot2(){
    var questions = Array();
    
    questions[0] = `
1.Antara berikut, manakah merupakan negeri-negeri melayu
  tidak bersekutu ?
    A.	Johor dan Perak
    B.	Kedah dan Perlis
    C.	Kedah dan Melaka
    D.	Kelantan dan Selangor
`;
    questions[1] = `
2.Antara berikut yang manakah bukan tergolong dalam kategori
  virus ?
    A.	Zika
    B.	Sallmonela
    C.	Denggi
    D.	Ebola
`;
    questions[2] = `
3.Penyakit demam denggi dibawa oleh nyamuk ______.
    A.	Anopheles
    B.	Mansonia
    C.	Aedes
    D.	Culex
`;
    questions[3] = `
4.Rajah diatas menunjukkan gambar bendera sebuah negara. 
  Apakah nama negara yang mewakili bendera tersebut?
    A.	Indonesia
    B.	Austria
    C.	Singapura
    D.	Poland
`;
    questions[4] = `
5.Pada tahun 1971 sehingga 1990, sebuah pembaharuan telah
  diperkenalkan oleh Tun Abdul Razak bagi mengurangkan
  kemiskinan dan menyusun semula struktur masyarakat. Apakah
  pembaharuan tersebut ?
    A.	Model Baru Ekonomi
    B.	Dasar Ekonomi Baru
    C.	Dasar Pembangunan negara
    D.	Transformasi Nasional 50
`;
    questions[5] = `
6.Negara manakah yang terkenal dengan tarian sufi?
    A.	Turki
    B.	Brazil
    C.	Sepanyol
    D.	Cuba

`;
    questions[6] = `
7.Rajah diatas menunjukkan simbol Pi dalam Matematik.
  Berapakah nilai sebenar Pi ?
    A.	3.14159265
    B.	3.12346778
    C.	3.15287392
    D.	3.26735408
`;
    questions[7] = `
8.Perkataan ‘Wiki’ yang membawa maksud ‘CEPAT’ berasal
  daripada bahasa apa?
    A.Bahasa Wales
    B. Bahasa Latin
    C.Bahasa Hawai
    D.Bahasa Finland 

`;
    questions[8] = `
  4W + 3 = -2(W + 3)
9.Cari nilai bagi W 
    A.	-3/2
    B.	5/6
    C.	3/2
    D.	-5/6

`;
    questions[9] = `
10.Namakan Negara yang mempunyai sempadan darat dengan
   Brazil,Guyana Perancis dan Guyana
    A.	Colombia
    B.	Paraguay
    C.	Suriname
    D.	Venezuela

`;
    questions[10] = `
11.Tarian Kathak merupakan salah satu tarian klasikal yang
   berasal dari _____.
    A.	Timur India
    B.	Utara India
    C.	Selatan India
    D.	Barat India

`;
    questions[11] = `
12.Apakah bentuk sistem ekonomi yang diguna pakai oleh
   negara Malaysia ?
    A.	Ekonomi Campuran
    B.	Ekonomi Kawalan
    C.	Ekonomi Islam
    D.	Ekonomi Kapitalis

`;
    questions[12] = `
13.Siapakah penulis buku komik MARVEL ? 
    A.	Michael Bay
    B.	Jon Favreau
    C.	Jon Watts
    D.	Stan Lee

`;
    questions[13] = `
14.Nama sebenar kartunis Malaysia,Dato’ Lat ialah
    A)	Mohd Nor Ali 		C) Mohd Nor Khalid
    B)	Mohd Nor Razif		D) Mohd Nor Lokman

`;
    questions[14] = `
15.Antara berikut yang manakah merupakan ibu negara Australia ?
    A.	Sydney
    B.	Brisbane
    C.	Canberra
    D.	Melbourne
`;
    questions[15] = `
16.Di dalam proses pemvulkanan,getah asli dipanaskan dengan bahan.
    A.	Karbon
    B.	Sulfur 
    C.	Silikon
    D.	Fosforus
`;
    generate(questions,"slot2-");
}

function slot3(){
    var questions = Array();
    
    questions[0] = `
1.Antara berikut ,negara manakah yang disenaraikan
  negara kesebelas terbaik di seluruh dunia dalam senarai
  Quality of Life Index ?
    A.	Australia
    B.	Amerika Syarikat
    C.	Singapura
    D.	Filipina
`;
    questions[1] = ``;
    questions[2] = `
3.Pada era manakah tarian Zapin menjadi popular ?
    A.	1970 – 1980 an
    B.	1930 – 1940 an
    C.	1975 – 1985 an
    D.	1950 – 1960 an
`;
    questions[3] = `
4.Daripada senarai di bawah, negara manakah yang bukan
  ahli dari Kesatuan Eropah (European Union)
    A.	Denmark
    B.	Norway
    C.	Sweden
    D.	Finland
`;
    questions[4] = `
5.Deklarasi Kuala Lumpur 1971 ialah satu persetujuan mengenai
    A.	Penubuhan ASEAN.
    B.	Penubuhan pasaran bersama untuk ASEAN.
    C.	Pakatan ketenteraan diantara negara-negara ASEAN.
    D.	Penubuhan Kawasan Aman, Bebas dan Berkecuali di
        Asia Tenggara.
`;
    questions[5] = `
6.Negara manakah yang pertama menjadi tuan rumah Sukan Sea?
    A)	Kemboja				C) Thailand
    B)	Timor-Leste 			D) Filipina
`;
    questions[6] = `
7.Dimanakah negara kelahiran composer terkenal Wolgang
  Amadeus Mozart ?
    A.	Jerman
    B.	Salzburg
    C.	Eisenach
    D.	Austria
`;
    questions[7] = `
8.Program Kebajikan Rakyat 1Malaysia (KAR1SMA) merupakan
  bantuan kepada kumpulan sasar yang terdiri daripada
    I	kanak-kanak               II	balu askar dan polis
    III	orang kelainan upaya      IV	mangsa bencana alam

    A.	I, II dan III        C.	II, III dan IV
    B.	I, II dan IV         D.	Semua diatas
`;
    questions[8] = `
        [Merah,Biru,Kuning,Hijau,Jingga,Ungu dan X]
    Pernyataan tersebut menunjukkan sebahagian daripada
          warna-warna yang terdapat pada pelangi.

9.Apakah warna yang mewakili huruf X?
    A)	Hitam				C) Toska
    B)	Indigo				D) Magenta
`;
    questions[9] = `
        Mamalia merujuk kepada haiwan yang terdiri
        daripada kelas vetebrata yang memiliki kelenjar
        susu yang digunakan untuk menyusui anaknya.

10.Apakah mamalia terkecil yang terdapat di dunia?
    A)	Kuda Thumbelina			C) Monyet Pygmy Marmoset
    B)	Kelawar Bumblebee 		D) Tikus kerdil Afrika
`;
    generate(questions,"slot3-");
}

var runonce = true;
function generate(questions, slot){
    if (runonce){
        $('#selectQuestion').append($("<option></option>").attr("value", "-").text("Select Question...")); 
        runonce = false;
    }
    $('#selectQuestion').append($("<option></option>").attr("value", "").text(slot + "---------")); 
    for (var i = 0; i < questions.length; i++){
        var txt = slot + "" + (i+1);
        $('#selectQuestion').append($("<option></option>").attr("value",questions[i]).text(txt)); 
    }
}