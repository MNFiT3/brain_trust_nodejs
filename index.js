var express = require("express");
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/');
});

var NumClientConn = 0;
var clientName = Array();
var ipAddress = Array();
var index = 0;
io.on('connection', function(socket){
	var address = socket.request.connection.remoteAddress;
	ipAddress[index] = address;
	socket.on('TeamName', function(msg){
        clientName[index] = msg;
		console.log(msg + ' connected from ' + address);
		/*
		console.log('clientName['+index+']= '+clientName[index]);
		console.log('ipAddress['+index+']= '+ipAddress[index]);
        */
		if (msg != "ADMIN"){
            NumClientConn++;
        }
        io.emit('NumClientConn', NumClientConn);
		io.emit('whosin', msg, address);
		index++;
    });
    
    socket.on('buttonClicked', function(msg){
        io.emit('buttonClicked', msg);
    });
    socket.on('question', function(question, QuestionNum){
        io.emit('question', question, QuestionNum);
    });
    socket.on('buttonReset', function(msg){
        io.emit('buttonReset', msg);
    });
    socket.on('disconnect', function(){
        var address = socket.request.connection.remoteAddress;
		var i = ipAddress.indexOf(address);
		var name = clientName[i];
		var msg = name+" ["+address+"]";
		console.log(name+' ('+address+') disconnected');
		if (i > -1) {
		clientName.splice(i, 1);	
		ipAddress.splice(i, 1);
		}
		index--;
		if (index < 0){
            index = 0;
        }
        NumClientConn--;
        if (NumClientConn < 0){
            NumClientConn = 0;
        }
        io.emit('NumClientConn', NumClientConn);
		io.emit('disconnect', msg);
    });
    socket.on('dispScore', function(Tname, Tscore){
        io.emit('dispScore', Tname, Tscore);
    });
	 socket.on('dispScoreFinal', function(TnameF, TscoreF){
        io.emit('dispScoreFinal', TnameF, TscoreF);
    });
	/*
	socket.on('dispScoreFinal', function(){
        io.emit('dispScoreFinal');
    });
	*/
    socket.on('AnswerResult', function(msg){
        io.emit('AnswerResult', msg);
    });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

